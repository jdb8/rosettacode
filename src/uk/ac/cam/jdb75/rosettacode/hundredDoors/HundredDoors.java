package uk.ac.cam.jdb75.rosettacode.hundredDoors;

/* See: http://rosettacode.org/wiki/100_doors */

public class HundredDoors {
    // Doors numbered from 0...100, in array of size 101
    private static boolean[] doors = new boolean[101];
    
    public static void main(String[] args) {
        // Loop through each time until we only look at the 100th door
        // First pass toggle every door, then 2,4,6..., then 3,6,9... etc.
        int pass = 1;
        while (pass <= 100) {            
            for (int i = 0; i <= 100; i += pass) {
                doors[i] = !doors[i];
            }
            pass++;            
        }
        
        // We can ignore door 0 (although in this case it is toggled to false)
        for (int j = 1; j <= 100; j++) {
            if (doors[j]) {
                System.out.println("Door " + j + " is open.");
            }
        }
        
    }
}
